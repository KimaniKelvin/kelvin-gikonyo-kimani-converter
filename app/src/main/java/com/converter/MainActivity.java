package com.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    EditText millimeters;
    TextView inches;
    Button convert, exit;
    Double in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        millimeters = findViewById(R.id.millimetersEdittext);
        inches = findViewById(R.id.inchesEdittext);
        convert = findViewById(R.id.convertbutton);
        exit = findViewById(R.id.exitbutton);

        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateForm()){

                    Double mm = Double.parseDouble(millimeters.getText().toString().trim());

                    in = mm/25.4;
                    String displayinches = Double.toString(in);
                    inches.setText(displayinches);

                }

            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private boolean validateForm() {

        boolean valid = true;

        String emptymm = millimeters.getText().toString().trim();

        if (emptymm.isEmpty()) {
            millimeters.setError("Cannot be empty");
            valid = false;
        }
         else {
            millimeters.setError(null);
        }

        return valid;
    }
}